#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include "DHT.h"
#include "ESP32_MailClient.h"

#include <PubSubClient.h>
#include <ArduinoJson.h>

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>

#include "time.h"

#define Door_sensor 17 // Digital pin input connected to GPIO_22
#define Fan_control 27 // Digital pin output connected to GPIO_19
#define Cua_dieu_khien 13// Digital Pin output connected to GPIO_18
#define ON 1
#define OFF 0
#define temp_in_max 37  //nguong nhiet do max cua cam bien trong phong
#define temp_out_max 34 //nguong nhiet do max cua cam bien ngoai
#define temp_in_min 31  //nguong nhiet do min cua cam bien trong phong
#define temp_out_min 28 //nguong nhiet do min cua cam bien ngoai

const char* ssid = "HYPERLOGY-Guest-02";
const char* password = "hyperlogy2019";
const char* ntpServer = "pool.ntp.org";

//@ir_dieu_khien
const uint16_t kIrLed = 12; // GPIO 12 ESP32
IRsend irsend(kIrLed);  // Set the GPIO to be used to sending the message.

/*@IR_data*/
//Power: On, Mode: 0 (Cool), Fan: 2 (Med), Temp: 25C, Zone Follow: Off, Sensor Temp: Off
uint16_t rawData[199] = {4330, 4400,  512, 1632,  512, 528,  542, 1630,  514, 1656,  486, 556,  514, 558,  514, 1656,  488, 584,  488, 558,  512, 1630,  512, 558,  512, 584,  488, 1656,  486, 1656,  488, 584,  486, 1654,  488, 556,  514, 1630,  514, 560,  512, 1630,  514, 1630,  512, 1630,  512, 1630,  514, 1630,  514, 1628,  540, 532,  514, 1628,  512, 558,  516, 582,  488, 556,  540, 556,  512, 558,  514, 1604,  512, 1630,  514, 556,  514, 556,  540, 530,  514, 584,  488, 582,  514, 530,  540, 532,  540, 530,  540, 1604,  538, 1630,  488, 1628,  542, 1602,  542, 1604,  540, 1630,  514, 5184,  4384, 4374,  538, 1606,  538, 558,  488, 1654,  514, 1630,  514, 530,  540, 532,  538, 1606,  538, 530,  540, 530,  540, 1630,  514, 556,  514, 532,  540, 1628,  514, 1628,  514, 506,  566, 1602,  540, 530,  542, 1602,  540, 532,  540, 1602,  540, 1628,  514, 1604,  538, 1630,  514, 1602,  542, 1604,  538, 530,  540, 1604,  538, 532,  540, 556,  514, 556,  514, 532,  540, 530,  542, 1604,  540, 1604,  540, 530,  542, 528,  542, 530,  540, 532,  540, 530,  540, 530,  540, 530,  540, 530,  542, 1602,  540, 1602,  540, 1602,  540, 1602,  540, 1602,  540, 1576,  568};
//Power: Off
uint16_t off_air[199] = {4378, 4402,  462, 1658,  510, 558,  512, 1658,  486, 1630,  512, 558,  512, 560,  510, 1632,  512, 584,  486, 534,  536, 1632,  510, 584,  486, 584,  462, 1682,  486, 1632,  486, 554,  542, 1632,  512, 532,  538, 1632,  510, 1632,  486, 1682,  462, 1682,  460, 586,  510, 1632,  486, 1658,  510, 1634,  510, 558,  512, 586,  462, 584,  486, 584,  486, 1656,  488, 584,  488, 584,  486, 1658,  510, 1632,  486, 1662,  482, 584,  488, 610,  462, 610,  484, 586,  462, 584,  486, 584,  510, 558,  488, 610,  462, 1656,  488, 1682,  462, 1656,  486, 1658,  486, 1656,  510, 5186,  4384, 4400,  512, 1658,  460, 584,  486, 1656,  512, 1658,  460, 584,  488, 584,  510, 1656,  462, 584,  486, 584,  488, 1682,  462, 582,  512, 532,  538, 1658,  460, 1658,  510, 560,  512, 1630,  488, 560,  536, 1630,  514, 1632,  512, 1632,  512, 1656,  462, 610,  462, 1682,  486, 1630,  514, 1630,  488, 584,  512, 556,  514, 530,  540, 584,  486, 1632,  514, 558,  512, 558,  512, 1656,  488, 1628,  512, 1656,  486, 560,  512, 582,  488, 556,  514, 558,  514, 532,  540, 556,  514, 560,  536, 532,  540, 1578,  538, 1630,  514, 1630,  512, 1628,  540, 1604,  514};
//@ir_dieu_khien


//@gui_email
#define emailSenderAccount    "hungnguyenrw@gmail.com"    
#define emailSenderPassword   "t0lachua"
#define emailRecipient        "dinhchinh42@gmail.com"
//#define emailRecipient        "dinhchinh42@gmail.com"
//#define emailRecipient2       "tung.pham@hyperlogy.com"
//#define emailRecipient3       "thang.le@hyperlogy.com"
#define smtpServer            "smtp.gmail.com"
#define smtpServerPort        465
#define emailSubject          "[Alert]Phòng Server"
//@gui_email

//@gui_email
SMTPData smtpData; // The Email Sending data object contains config and data to send
//void sendCallback(SendStatus info); // Callback function to get the Email sending status
uint8_t email_noti = 0;
//@gui_email

//@nhiet_do_va_do_am
#define DHTPIN_in 0     // Digital pin connected to the DHT sensor ~ GPIO 4ESP32 for ESP32
#define DHTPIN_out 4     // Digital pin connected to the DHT22 sensor ~ GPIO 0 for 
#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE_2 DHT11 // DHT 11
//@nhiet_do_va_do_am

//@nhiet_do_va_do_am
DHT dht_in(DHTPIN_in, DHTTYPE);
DHT dht_out(DHTPIN_out, DHTTYPE_2);
//@nhiet_do_va_do_am  

//@MQTT
#define mqtt_server "broker.hivemq.com" 
const uint16_t mqtt_port = 1883;
WiFiClient espClient;
PubSubClient client(espClient);
char a[100];
int status_fan = 1;
int led_fan = 0, led_air = 0, led_door = 0;
void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.print("Co tin nhan moi tu topic:");
  Serial.println(topic);
  for (int i = 0; i < length; i++){
    a[i] = payload[i];
  }
  if(a[0] == 49)
  {
    Cua_trang_thai(ON);
    led_door = ON;
  }
  if(a[0] == 50)
  {
    Cua_trang_thai(OFF);
    led_door = OFF;
  }
  if(a[0] == 51)
  {
    bat_tat_dieu_hoa(ON);
    led_air = ON;
  }
  if(a[0] == 52)
  {
    bat_tat_dieu_hoa(OFF);
    led_air = OFF;
  }
  if(a[0] == 53)
  {
    //status_fan = ON;
    led_fan = ON;
    Quat_dieu_khien(ON);
  }
  if(a[0] == 54)
  {
    //status_fan = OFF;
    led_fan = OFF;
    Quat_dieu_khien(OFF);
  }
  Serial.println(a);
}
void reconnect() 
{
  while (!client.connected()) // Chờ tới khi kết nối
  {
    // Thực hiện kết nối với mqtt user và pass
    if (client.connect("123","Topic_Offline",0,0,"Thietbioutoffine"))  //kết nối vào broker
    {
      Serial.println("Đã kết nối:");
      client.subscribe("Hyperlogy_Server");
    }
    else 
    {
      Serial.print("Lỗi:, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
       //Đợi 5s
      delay(5000);
    }
  }
}
//@MQTT

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  setup_wifi(); 
  sendMail_init();
  pinMode(Fan_control, OUTPUT);
  pinMode(Cua_dieu_khien, OUTPUT);
  pinMode(Door_sensor, INPUT);
  irsend.begin(); //@ir_dieu_khien
  dht_in.begin();
  dht_out.begin();
  //mqtt
  client.setServer(mqtt_server, mqtt_port); 
  client.setCallback(callback);
  //mqtt
}

int status_temp = 0; //trang thai doi bien khi nhiet do >37
int status_temp1 = 0; //trang thai doi bien khi nhiet do <31
int time_fan = 0; //bien dem thoi gian cua quat
int time_temp=0;
#define time_delay_temp 5000 //thoi gian 5s doc cam bien 1 lan
#define fan_restTime 720 //dem 720 lan, moi lan 5s thi dc la 3600s tuc la quat chay 1 tieng thi nghi 5 phut
#define fan_wakeTime 780 //dem 780 lan, moi lan 5s thi dc la 3900s tuc la quat chay 1 tieng thi nghi 5 phut roi lai bat

void loop() {
  // put your main code here, to run repeatedly:
  //mqtt
  if (!client.connected())// Kiểm tra kết nối
      reconnect();
  client.loop();
  //mqtt
  float dht_in_t;
  float dht_out_t;
  if(time_fan == 0)
  {
    Quat_dieu_khien(ON);
    led_fan = ON;
  }
  if(millis() > time_temp + time_delay_temp) //5s doc 1 lan nhiet do
  {
    time_temp = millis();
    time_fan++;
    dht_in_t = dht_in.readTemperature();//DHT (in)
    dht_out_t = dht_out.readTemperature();//DHT (out)
    //mqtt
    StaticJsonDocument<200> doc;
    doc["Temperature_in"] = dht_in_t;
    doc["Temperature_out"] = dht_out_t;
    doc["Status_Fan"]= led_fan;
    doc["Status_Air"]= led_air;
    doc["Status_Door"]= led_door;
    char buffer[256];
    size_t n = serializeJson(doc, buffer);
    client.publish("Hyper123456", buffer, n);
    serializeJsonPretty(doc, Serial);
    //mqtt
    Serial.print(F("\nTemp in: "));
    Serial.println(dht_in_t);

    Serial.print(F("Temp out: "));
    Serial.println(dht_out_t);
  } 
  if(time_fan == fan_restTime)  //1 tieng thi tat quat
  {
    Quat_dieu_khien(OFF);
    led_fan = OFF;
  }
  if(time_fan == fan_wakeTime) //tat quat roi thi nghi 5 phut va lai bat quat
  { 
    Quat_dieu_khien(ON);
    time_fan = 0;
    led_fan = ON;
  }
  if(dht_in_t > temp_in_max || dht_out_t > temp_out_max)
  {
    if(status_temp == 0)
    {
      status_temp1 = 0;
      bat_tat_dieu_hoa(ON);
      Cua_trang_thai(OFF); //dong cua
      led_air = 1;
      led_door = 0;
      status_temp = 1;
    }
  }
  if(dht_in_t < temp_in_min)
  {
    if(status_temp1 == 0)
    {
      status_temp = 0;
      bat_tat_dieu_hoa(OFF);
      Cua_trang_thai(ON); //mo cua
      led_air = 0;
      led_door = 1;
      status_temp1 = 1;
    }
  }
}

void bat_tat_dieu_hoa(bool state)
{
  if(state == 1 && email_noti == 0)
  {
    irsend.sendRaw(rawData, 199, 38);
    MailClient.sendMail(smtpData);
    Serial.println("Air on");
  }
  if(state == 0)
  {
    irsend.sendRaw(off_air, 199, 38);
    Serial.println("Air off");
  }  
}

//@Quat_dieu_khien
void Quat_dieu_khien(bool state)
{
  if(state == 1)
  {
    digitalWrite(Fan_control, HIGH);
    //Serial.println(F("Quat ON"));
  }
  else
  {
    digitalWrite(Fan_control, LOW);
    //Serial.println(F("Quat OFF"));
    
  }
}

//@Cua_dieu_khien
void Cua_trang_thai(bool state)
{
  if(state == 0) // Dong Cua
  {
    digitalWrite(Cua_dieu_khien, HIGH);
    Serial.println(F("Cua CLOSED"));
  }
  else // Mo Cua
  {
    digitalWrite(Cua_dieu_khien, LOW);
    Serial.println(F("Cua OPEN"));    
  }
}

void setup_wifi()
{
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  static int time_delay_wifi = 2000;
  static int time_wifi= 0;
  while (WiFi.waitForConnectResult() != WL_CONNECTED) 
  {
     Serial.println("Connection Failed! Rebooting...");
     if(millis() > time_wifi + time_delay_wifi)
     {
        time_wifi = millis();
        ESP.restart();
     }
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());;
}

void sendMail_init()
{
  Serial.println("Preparing to send email");
  Serial.println();
  smtpData.setLogin(smtpServer, smtpServerPort, emailSenderAccount, emailSenderPassword);
  smtpData.setSender("Phòng Server - Hyperlogy", emailSenderAccount);
  // Set Email priority or importance High, Normal, Low or 1 to 5 (1 is highest)
  smtpData.setPriority("High");
  // Set the subject
  smtpData.setSubject(emailSubject);
  // Set the message with HTML format
  smtpData.setMessage("<div style=\"color:#2f4468;\"><h1>Đã bật điều hòa, temp > 35</h1><p>- Gửi từ Phòng Server Hyperlogy</p></div>", true);
  // Set the email message in text format (raw)
  // Add recipients, you can add more than one recipient
  smtpData.addRecipient(emailRecipient);
  //smtpData.addRecipient("YOUR_OTHER_RECIPIENT_EMAIL_ADDRESS@EXAMPLE.com");
  //smtpData.setSendCallback(sendCallback);
  //@gui_email
}
